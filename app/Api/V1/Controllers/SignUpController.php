<?php

namespace App\Api\V1\Controllers;

use App\Http\Middleware\UserMiddleware;
use Config;
use App\User;
use http\Env\Response;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $user = new User($request->all());
        $check_user=User::where('email',$user->email)->first();
        if ($check_user) {
            return response()->json(['success'=>false,'message'=>'User Already Exists'],406);
        }
        if(!$user->save()) {
            throw new HttpException(500);
        }


        if(!Config::get('boilerplate.sign_up.release_token')) {
            return response()->json([
                'success' => true,
                'message'=>'User Signed Up Successfully'
            ], 201);
        }

        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'success' => true,
            'token' => $token
        ], 201);
    }
}
